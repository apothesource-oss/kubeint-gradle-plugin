[![Gradle Plugin Portal](https://img.shields.io/maven-metadata/v/https/plugins.gradle.org/m2/com/apothesource/fam/kubeint/itest/com.apothesource.fam.kubeint.itest.gradle.plugin/maven-metadata.xml.svg?colorB=007ec6&label=gradle)](https://plugins.gradle.org/plugin/com.apothesource.fam.kubeint.itest)

# KubeInt Gradle Plugin

kubeint-gradle-plugin is a gradle plugin designed to stand up a set of containers in kubernetes, run integration
tests defined in `/src/itest/java` and then tear down the containers. The integration test portion is based off of 
[gradle-integration-test-plugin](https://github.com/coditory/gradle-integration-test-plugin)

## Plugin Use
In order to use the plugin you will need to add the following to the top of your project's `build.gradle` file:
```groovy
plugins {
  id "com.apothesource.fam.kubeint.itest" version "0.2.0"
}
```
or 
```groovy
buildscript {
    repositories {
        url "https://plugins.gradle.org/m2/"
    }
    dependencies {
        classpath 'com.apothesource.fam:kubeint-gradle-plugin:0.2.0'
    }
}

apply plugin: 'com.apothesource.fam.kubeint.itest'
```
Note: Use `mavenLocal()` for the repository if building and publishing locally with  `./gradlew publishToMavenLocal`

A sample configuration for the plugin in the `build.gradle` would look like:
```groovy
tasks.withType(Test) {
    it.useJUnitPlatform()
    it.testLogging {
        events = ["PASSED",
                  "FAILED",
                  "SKIPPED"]
        exceptionFormat = 'full'
    }
}

itest {
    projectImage = jib.to.image

    service {
        service = "my-service-v2"   // must match the k8s pod `service` label
        remotePort = 8080
        localPort = 31075
    }
}
```

If running multiple tasks in a single build, you may want to ensure that the jib.to.image is created before tests run with:
```groovy
tasks.deployServices.dependsOn(tasks.jib)
```

### Add DTR secret to kubernetes
In order to use the plugin, Kubernetes needs the DTR secret to pull down images stored in the remote
DTR. The secret must be created in the default namespaces to be reused in the test namespace. 

#### For a docker registry (that uses docker login)

You will need to do a `docker login` to the remote DTR, then run the following command:

```shell
kubectl create secret generic docker-config  \
  --from-file=.dockerconfigjson=<pathToDockerConfigDir>/.docker/config.json \
  --type=kubernetes.io/dockerconfigjson
```

#### For Artifact Registry service account

```shell
kubectl create secret docker-registry docker-config \
  --docker-server=https://LOCATION-docker.pkg.dev \
  --docker-email=SERVICE-ACCOUNT-EMAIL \
  --docker-username=_json_key \
  --docker-password="$(cat KEY-FILE)"
```


### Service Under Test
The plugin configuration defines the service-under-test (SUT). The plugin will substitute the placeholder string
<projectImage> for the SUT image, and the plugin will use the value of the `service` configuration and the `service`
flag to create a k8s service manifest for the SUT.

### Configuration
The following are configuration properties you can use in the `itest` config in a project's build.gradle file:

    namespace: Name of the test namespace where services will be deployed to, defaults to `$project.name-test`
    projectImage: If provided, will overwrite `<projectImage>` in kube.yaml, allows for not specifying the main project
        can use `projectImage = jib.to.image` to automatically use the newly built jib image
    testFile: Defaults to kube.yaml # This file needs to be located in `src/itest/resources
    deployTimeout: Defaults to 120 seconds
    deleteTimeout: Defaults to 120 seconds
    service: the service under test

### Service Value Enum
The plugin will create a a`KubeIntServiceValues.java` enum class that defines how the runtime address of the SUT. This file
should be committed to version control.

### Command line flags
Below are a list of command line flags supported by the plugin (`gradle build -D<flag>`):
* `saveTestNamespace`: Skips the `deleteNamespace` task after the tests have run in order to look at service logs
* `skipItests`: Skips integration tests
* `skipUnitTests`: Skips unit tests
* `skipTests`: Skips both unit and integration tests
* `skipTestDeploy`: Skips the deployment task in order to run integration tests against a service already stood up
* `service`: One of `forward`, `clusterip` or `nodeport`. If none porvided, defaults to `nodeport`.

### Tasks
Below are a list of the tasks that are registered by this plugin:
* `itest`: Builds the local docker image, stands up the namespace, runs integration tests, and tears down the namespace
* `deployServices`: Stands up the namespace and deploys the containers from the kube.yaml

