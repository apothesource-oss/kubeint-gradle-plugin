package %package%;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.annotation.Generated;

@Generated(value = "%generatorClass%", comments = "Enum values to use in tests")
public enum KubeIntServiceValues {

    SERVICE_ADDRESS;

    private static final String PROPERTIES_FILE_NAME = "services.properties";
    private static final Properties properties;

    public String getValue() {
        String propertyValue = (String) properties.get(this.name());

        if (propertyValue == null) {
            System.out.println("Property for " + this.name() + " was null!");
        }
        return propertyValue;
    }

    static {
        try (InputStream input = KubeIntServiceValues.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties = new Properties();
            properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException("Exception while trying to read properties file: " + PROPERTIES_FILE_NAME, e);
        }
    }

}