package com.apothesource.fam.kubeint

import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.CREATE_NAMESPACE
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.DELETE_NAMESPACE
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.DEPLOY_SERVICES
import io.fabric8.kubernetes.api.model.*
import io.fabric8.kubernetes.client.*
import org.apache.commons.io.IOUtils
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.internal.logging.events.LogEvent
import org.gradle.internal.logging.events.OutputEvent
import org.gradle.internal.logging.events.OutputEventListener
import org.gradle.internal.logging.slf4j.OutputEventListenerBackedLoggerContext
import org.slf4j.LoggerFactory
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis

internal object KubernetesTaskConfiguration {
    private const val GROUP = "Kubernetes Integration Tasks"
    private var testNamespace = Namespace()
    private var process: Process? = null

    /**
     * Registers the different tasks for running setting up/tearing down kubernetes.
     */
    fun registerTasks(project: Project, sourceSet: SourceSet, extension: KubeIntExtension) {
        disableFabric8Logging()
        registerInitialDeleteNamespace(project, extension)
        registerDeleteNamespace(project, extension)
        registerCreateNamespace(project, extension)
        registerDeployServices(project, sourceSet, extension)
    }

    /**
     * Deletes the test namespace if it was still around, and sets up the namespace object.
     * This should be the first task to run for the integration tests
     */
    private fun registerInitialDeleteNamespace(project: Project, extension: KubeIntExtension) {
        project.tasks.register("${DELETE_NAMESPACE}IfExists") { task ->
            task.group = GROUP
            task.description = "Deletes an existing namespace if it exists before creating the new namespace"
            task.onlyIf { !KubeIntPropertyHandler.skipIntegrationTest() }
            task.doLast {
                DefaultKubernetesClient().use { client ->
                    testNamespace = NamespaceBuilder()
                        .withNewMetadata()
                        .withName(extension.getNamespace())
                        .endMetadata()
                        .build()

                    deleteNamespace(client, extension, testNamespace)
                }
            }
        }
    }

    /**
     * Creates the test namespace to be used by the integration tests and copies over the
     * DTR secret from the default namespace in order to pull down the deployment images
     */
    private fun registerCreateNamespace(project: Project, extension: KubeIntExtension) {
        project.tasks.register(CREATE_NAMESPACE) { task ->
            task.group = GROUP
            task.description = "Creates the namespace defined by itest.namespace and copies the DTR secret"
            task.dependsOn("${DELETE_NAMESPACE}IfExists")
            task.onlyIf { !KubeIntPropertyHandler.skipIntegrationTest() }
            task.doLast {
                println("Creating namespace ${extension.getNamespace()}...")
                DefaultKubernetesClient().use { client ->
                    client.namespaces().create(testNamespace)
                    val dtrSecret = client.secrets().inNamespace("default")
                        .withName("docker-config").get()

                    val testSecret = SecretBuilder()
                        .withNewMetadata().withName("docker-config").endMetadata()
                        .addToData(dtrSecret.data)
                        .withType(dtrSecret.type)
                        .build()
                    client.secrets().inNamespace(extension.getNamespace()).create(testSecret)
                }
            }
        }
    }

    /**
     * Creates the task to deploy the services defined in kube.yaml to the test namespace.
     *
     *  Timeout throws errors in the log but does not effect the results, issue filed at:
     *  https://github.com/fabric8io/kubernetes-client/issues/2956
     */
    private fun registerDeployServices(project: Project, sourceSet: SourceSet, extension: KubeIntExtension) {
        project.tasks.register(DEPLOY_SERVICES) { task ->
            task.group = GROUP
            task.description = "Deploys the ${extension.getTestFile()} found in 'src/itest/resources/'"
            task.dependsOn(CREATE_NAMESPACE)
            task.onlyIf { !KubeIntPropertyHandler.skipIntegrationTest() }
            task.doLast {
                DefaultKubernetesClient().use { client ->
                    try {
                        val fileInputStream = getKubeYaml(extension, sourceSet)

                        val loadNamespaceMs = measureTimeMillis {
                            client.load(fileInputStream)
                                .inNamespace(extension.getNamespace())
                                .createOrReplaceAnd()
                                .withWaitRetryBackoff(1, TimeUnit.SECONDS, 1.0)
                                .waitUntilReady(extension.getDeployTimeout(), TimeUnit.SECONDS)
                        }

                        val loadNamespaceSeconds = loadNamespaceMs / 1000.0
                        println("${extension.getTestFile()} loaded in ${loadNamespaceSeconds}s")

                        when (KubeIntPropertyHandler.serviceMode()) {
                            KubeIntPropertyHandler.ServiceMode.CLUSTERIP -> doClusterIp(extension, client)
                            KubeIntPropertyHandler.ServiceMode.FORWARD -> doPortForward(extension, client)
                            KubeIntPropertyHandler.ServiceMode.NODEPORT -> doNodePort(extension, client)
                        }

                        // wait or every pod to be ready before completing this task
                        val readyTimeMs = measureTimeMillis {
                            client.pods().inNamespace(extension.getNamespace()).list().items
                                .forEach {
                                    client.resource(it)
                                        .withWaitRetryBackoff(1, TimeUnit.SECONDS, 1.0)
                                        .waitUntilReady(
                                            extension.getDeployTimeout() - loadNamespaceSeconds.toInt(),
                                            TimeUnit.SECONDS
                                        )
                                }
                        }

                        println("Pods ready in ${readyTimeMs/1000.0}s")
                    } catch (e: KubernetesClientTimeoutException) {
                        throw GradleException(
                            "Timed out trying to deploy the ${extension.getTestFile()} after ${extension.getDeployTimeout()} seconds",
                            e
                        )
                    }

                    println("=======Pods=======")
                    client.pods().inNamespace(extension.getNamespace()).list().items.forEach {
                        val readyStatus = it.status.conditions.find { condition -> condition.type.equals("Ready") }!!.status
                        println("${it.metadata.name} : Ready=${readyStatus}")
                    }


                    // wait up to 30s for service to be reachable
                    val url = URL(KubeIntPropertyHandler.getServiceAddressSystemProperty(extension))
                    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
                    connection.connectTimeout = 1000

                    val startTime = System.currentTimeMillis()
                    var healthy = false
                    while (!healthy && System.currentTimeMillis() - startTime < 30 * 1000) {
                        try {
                            connection.connect()
                            healthy = true
                        } catch (e: IOException) {
                            // ignore
                        }
                    }

                    val elapsedSeconds = (System.currentTimeMillis() - startTime) / 1000.0
                    if (healthy) {
                        println("Service healthy after ${elapsedSeconds}s")
                    } else {
                        println("$url unreachable after ${elapsedSeconds}s")
                    }
                }
            }
        }
    }
    
    private fun doNodePort(extension: KubeIntExtension, client: DefaultKubernetesClient) {
        client.services().inNamespace(extension.getNamespace())
            .createOrReplace(serviceBuilder(extension)
                .editSpec()
                    .withType("NodePort")
                    .editFirstPort()
                        .withNodePort(extension.getService().getLocalPort())
                        .endPort()
                    .endSpec()
                .build())
    }

    private fun doClusterIp(extension: KubeIntExtension, client: KubernetesClient) {
        client.services().inNamespace(extension.getNamespace())
            .createOrReplace(serviceBuilder(extension)
                .editSpec()
                    .withType("ClusterIP")
                    .endSpec()
                .build())
    }

    private fun doPortForward(extension: KubeIntExtension, client: KubernetesClient) {
        doClusterIp(extension, client)
        val service = extension.getService()
        println("Forwarding local port ${service.getLocalPort()} to ${service.getRemotePort()}.")
        process = ProcessBuilder(
            "kubectl",
            "port-forward",
            "service/${service.getName()}",
            String.format("%d:%d", service.getLocalPort(), service.getRemotePort()),
            "-n", extension.getNamespace())
            .inheritIO().start()
    }

    private fun serviceBuilder(extension: KubeIntExtension): ServiceBuilder {
        return ServiceBuilder().withNewMetadata()
                .withName(extension.getService().getName())
                .addToLabels("service", extension.getService().getName())
            .endMetadata()
            .withNewSpec()
                .addNewPort()
                    .withName("http-${extension.getService().getRemotePort()}")
                    .withProtocol("TCP")
                    .withPort(extension.getService().getRemotePort())
                    .withNewTargetPort(extension.getService().getRemotePort())
                .endPort()
                    .withSelector<String, String>(mapOf("service" to extension.getService().getName()))
                    .withSessionAffinity("None")
            .endSpec()
    }
    /**
     * Deletes the test namespace whether or not the build was successful
     * This should be the last task to run for the integration tests
     */
    fun registerDeleteNamespace(project: Project, extension: KubeIntExtension) {
        project.tasks.register(DELETE_NAMESPACE) { task ->
            task.group = GROUP
            task.description = "Deletes the namespace once the integration tests have completed"
            task.onlyIf { project.tasks.getByName(DEPLOY_SERVICES).didWork && !KubeIntPropertyHandler.saveTestNamespace() }
            task.doLast {
                DefaultKubernetesClient().use { client -> deleteNamespace(client, extension, testNamespace) }

                process?.let {
                    if (it.isAlive) {
                        it.destroyForcibly().waitFor(5, TimeUnit.SECONDS);
                    }
                }
            }
        }
    }

    /**
     * Deletes the provided namespace if it exists.
     */
    private fun deleteNamespace(client: DefaultKubernetesClient, extension: KubeIntExtension, testNamespace: Namespace) {
        val namespaceExists = client.namespaces().list().items.stream()
            .anyMatch { e -> e.metadata.name.equals(extension.getNamespace()) }
        if (namespaceExists) {
            process?.let {
                if (it.isAlive) {
                    it.destroyForcibly().waitFor(5, TimeUnit.SECONDS);
                }
            }

            println("Deleting namespace ${extension.getNamespace()}...")
            val deleteLatch = CountDownLatch(1)

            client.namespaces().delete(testNamespace)
            val watch = client.namespaces().watch(namespaceWatcher(deleteLatch))
            deleteLatch.await(extension.getDeleteTimeout(), TimeUnit.SECONDS)
            watch.close()
        }
    }

    /**
     * This will get the kube.yaml and replace <projectImage> if one is provided through the extension,
     * or just use the file as is if no project image is provided
     */
    private fun getKubeYaml(extension: KubeIntExtension, sourceSet: SourceSet): InputStream {
        val originalFile = FileInputStream(sourceSet.resources.files
            .first { it.name.contains(extension.getTestFile()) })
        return if (extension.getProjectImage().isNotEmpty()) {
            val content = IOUtils.toString(originalFile, "UTF-8")
                .replace("<projectImage>", extension.getProjectImage().toString())
            content.byteInputStream()
        } else {
            originalFile
        }
    }

    /**
     * Creates a watcher to monitor when a namespace has been fully deleted
     */
    private fun namespaceWatcher(deleteLatch: CountDownLatch) = object : Watcher<Namespace> {
        override fun eventReceived(action: Watcher.Action?, resource: Namespace?) {
            if (action == Watcher.Action.DELETED) {
                deleteLatch.countDown()
            }
        }

        override fun onClose(cause: WatcherException?) {
            TODO("Not Needed")
        }
    }


    private fun serviceAddedWatcher(createdLatch: CountDownLatch, serviceName: String) = object : Watcher<Service> {
        override fun eventReceived(action: Watcher.Action?, resource: Service?) {
            if (action == Watcher.Action.ADDED && serviceName == resource?.metadata?.name) {
                createdLatch.countDown()
            }
        }

        override fun onClose(cause: WatcherException?) {
            TODO("Not Needed")
        }
    }

    /**
     * Fabric8 has a lot of warnings from WatcherWebSocketListener that clutters the log, this disables them
     */
    private fun disableFabric8Logging() {
        val context = LoggerFactory.getILoggerFactory() as OutputEventListenerBackedLoggerContext
        val defaultOutputEventListener = context.outputEventListener
        context.outputEventListener = OutputEventListener { event: OutputEvent ->
            val logEvent = event as LogEvent
            if (!logEvent.category.contains("io.fabric8")) {
                defaultOutputEventListener.onOutput(event)
            }
        }
    }
}