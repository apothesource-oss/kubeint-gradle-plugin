package com.apothesource.fam.kubeint

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginConvention
import java.io.*
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.util.*
import java.util.stream.Collectors

object KubeIntResources {

    private const val CLASS_NAME: String = "KubeIntServiceValues.java"

    fun createResources(project: Project) {
        val srcDir = project.convention
            .getPlugin(JavaPluginConvention::class.java)
            .sourceSets
            .getByName("itest")
            .java
            .srcDirs
            .first()
            .toPath()

        val resourceDir = project.convention
            .getPlugin(JavaPluginConvention::class.java)
            .sourceSets
            .getByName("itest")
            .output
            .resourcesDir!!.toPath()

        val extension = project.extensions.getByType(KubeIntExtension::class.java)
        val classAsString = getClassAsString(extension)
        val srcFile = Path.of("${srcDir.toAbsolutePath()}/${project.group.toString().replace(".", "/")}/$CLASS_NAME")

        Files.createDirectories(srcFile.parent)
        Files.writeString(srcFile, classAsString)

        val resourceFileName = Path.of("${resourceDir.toAbsolutePath()}/services.properties")

        Files.createDirectories(resourceFileName.parent)
        createPropertiesFile(
            mapOf("SERVICE_ADDRESS" to KubeIntPropertyHandler.getServiceAddressSystemProperty(extension)),
            resourceFileName.toAbsolutePath().toString())
    }

    private fun getClassAsString(extension: KubeIntExtension): String {
        val inputStream = this.javaClass.classLoader.getResourceAsStream(CLASS_NAME) ?: throw RuntimeException("Unable to retrieve k8s service values enum template!")
        val classFileTemplate = BufferedReader(InputStreamReader(inputStream, StandardCharsets.UTF_8))
            .lines()
            .collect(Collectors.joining("\n"))
        return classFileTemplate.replace("%generatorClass%", this.javaClass.canonicalName)
            .replace("%package%", "io.apothesource.fam")
            .replace("%serviceAddress%", KubeIntPropertyHandler.getServiceAddressSystemProperty(extension))
    }

    private fun createPropertiesFile(propertiesMap: Map<String, String>, resourceFileName: String) {
        val properties = Properties()
        properties.putAll(propertiesMap)
        properties.store(FileOutputStream(resourceFileName), null)
    }

}
