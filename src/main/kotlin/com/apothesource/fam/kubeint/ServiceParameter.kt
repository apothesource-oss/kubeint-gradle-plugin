package com.apothesource.fam.kubeint

import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import javax.inject.Inject


open class ServiceParameter @Inject constructor(objectFactory: ObjectFactory) {
    private val name: Property<String>
    private val localPort: Property<Int>
    private val remotePort: Property<Int>

    @Input
    fun getName(): String {
        return name.get()
    }

    fun setName(name: String) {
        this.name.set(name)
    }

    @Input
    fun getLocalPort(): Int {
        return localPort.get()
    }

    fun setLocalPort(localPort: Int) {
        this.localPort.set(localPort)
    }

    @Input
    fun getRemotePort(): Int {
        return remotePort.get()
    }

    fun setRemotePort(remotePort: Int) {
        this.remotePort.set(remotePort)
    }

    init {
        name = objectFactory.property(String::class.java)
        localPort = objectFactory.property(Int::class.java)
        remotePort = objectFactory.property(Int::class.java)
    }
}
