package com.apothesource.fam.kubeint

import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SAVE_TEST_NAMESPACE
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SERVICE_MODE
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SKIP_INTEGRATION_TEST_FLAG_NAME
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SKIP_TEST_ALL_FLAG_NAME
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SKIP_TEST_DEPLOY
import com.apothesource.fam.kubeint.KubeIntPlugin.Companion.SKIP_TEST_FLAG_NAME

/**
 * Handles skipping of unit and integration tests with system command line arguments.
 */
internal object KubeIntPropertyHandler {

    enum class ServiceMode {
        CLUSTERIP, NODEPORT, FORWARD
    }

    fun serviceMode(): ServiceMode {
        return toServiceMode(System.getProperty(SERVICE_MODE))
    }

    fun toServiceMode(string: String?): ServiceMode {
        return ServiceMode.values().find { it.name.equals(string, ignoreCase = true) } ?: ServiceMode.NODEPORT
    }

    fun getServiceAddressSystemProperty(extension: KubeIntExtension): String {
        when (this.serviceMode()) {
            ServiceMode.CLUSTERIP -> return "http://${extension.getService().getName()}.${extension.getNamespace()}:${extension.getService().getRemotePort()}"
            ServiceMode.NODEPORT -> return "http://localhost:${extension.getService().getLocalPort()}"
            ServiceMode.FORWARD -> return "http://localhost:${extension.getService().getLocalPort()}"
        }
    }

    fun skipTestAll(): Boolean {
        return hasSkipAllFlag() ||
            (hasSkipTestFlag() && hasSkipIntegrationTestFlag())
    }

    fun skipTest(): Boolean {
        return hasSkipAllFlag() || hasSkipTestFlag()
    }

    fun skipIntegrationTest(): Boolean {
        return hasSkipAllFlag() || hasSkipIntegrationTestFlag()
    }

    fun saveTestNamespace(): Boolean {
        return hasPropertyFlag(SAVE_TEST_NAMESPACE)
    }

    /**
     * Used to skip the deploy state in order to run integration tests against an already deployed set of services.
     */
    fun skipTestDeploy(): Boolean {
        return hasPropertyFlag(SKIP_TEST_DEPLOY)
    }

    private fun hasSkipAllFlag(): Boolean {
        return hasPropertyFlag(SKIP_TEST_ALL_FLAG_NAME)
    }

    private fun hasSkipTestFlag(): Boolean {
        return hasPropertyFlag(SKIP_TEST_FLAG_NAME)
    }

    private fun hasSkipIntegrationTestFlag(): Boolean {
        return hasPropertyFlag(SKIP_INTEGRATION_TEST_FLAG_NAME)
    }


    private fun hasPropertyFlag(name: String, default: Boolean): Boolean {
        if (System.getProperty(name) != null) {
            val value = System.getProperty(name)
            return value == null || !value.toString().equals("false", true)
        }
        return default
    }

    private fun hasPropertyFlag(name: String): Boolean {
        return hasPropertyFlag(name, false)
    }
}
