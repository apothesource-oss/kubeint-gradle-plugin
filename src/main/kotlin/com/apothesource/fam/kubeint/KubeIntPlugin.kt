package com.apothesource.fam.kubeint

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.testing.Test
import org.gradle.language.base.plugins.LifecycleBasePlugin


open class KubeIntPlugin : Plugin<Project> {

    override fun apply(project: Project) {
        if (!project.plugins.hasPlugin(JavaPlugin::class.java)) {
            project.plugins.apply(JavaPlugin::class.java)
        }

        val sourceSet = TestTaskConfiguration.apply(project)
        val extension = project.extensions.create(EXTENSION_NAME, KubeIntExtension::class.java, project)

        KubernetesTaskConfiguration.registerTasks(project, sourceSet, extension)
        registerIntegrationTestTask(project, sourceSet)
    }

    // TODO: move to a task object
    private fun registerIntegrationTestTask(project: Project, sourceSet: SourceSet) {
        val integrationTest = project.tasks.register(ITEST_TASK_NAME, Test::class.java) { task ->
            task.description = "Runs the ${ITEST_TASK_NAME}s."
            task.group = LifecycleBasePlugin.VERIFICATION_GROUP
            task.testClassesDirs = sourceSet.output.classesDirs
            task.classpath = sourceSet.runtimeClasspath
            task.mustRunAfter(JavaPlugin.TEST_TASK_NAME)
            task.onlyIf { !KubeIntPropertyHandler.skipIntegrationTest() }

            if (!KubeIntPropertyHandler.saveTestNamespace()) {
                task.finalizedBy(DELETE_NAMESPACE)
            }
        }

        if (!KubeIntPropertyHandler.skipIntegrationTest() && !KubeIntPropertyHandler.skipTestDeploy()) {
            integrationTest.configure { it.dependsOn(DEPLOY_SERVICES) }
        }

        project.tasks.getByName(JavaBasePlugin.CHECK_TASK_NAME)
            .dependsOn(integrationTest)
    }

    companion object {
        const val EXTENSION_NAME = "itest"

        const val ITEST_TASK_NAME = "itest"
        const val TEST_ALL_TASK_NAME = "testAll"
        const val SKIP_TEST_FLAG_NAME = "skipUnitTests"
        const val SKIP_TEST_ALL_FLAG_NAME = "skipTests"
        const val SKIP_INTEGRATION_TEST_FLAG_NAME = "skipItests"
        const val SAVE_TEST_NAMESPACE = "saveTestNamespace"
        const val SKIP_TEST_DEPLOY = "skipTestDeploy"
        const val SERVICE_MODE = "service"

        const val CREATE_NAMESPACE = "createNamespace"
        const val DEPLOY_SERVICES = "deployServices"
        const val DELETE_NAMESPACE = "deleteNamespace"
    }

}