package com.apothesource.fam.kubeint

import com.apothesource.fam.kubeint.KubeIntPropertyHandler.skipTest
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.testing.Test
import org.gradle.language.base.plugins.LifecycleBasePlugin
import java.io.File

internal object TestTaskConfiguration {
    fun apply(project: Project): SourceSet {
        testTaskConfiguration(project)
        testAllTaskConfiguration(project)
        val sourceSet = setupSourceSet(project)
        setupConfiguration(project)
        processTaskConfiguration(project)
        return sourceSet
    }

    private fun processTaskConfiguration(project: Project) {
        project.tasks.getByName("processItestResources")
            .doLast {
                KubeIntResources.createResources(project)
            }
    }

    /**
     * Sets up the source set to look for integration test in `/src/itest/java`.
     */
    private fun setupSourceSet(project: Project): SourceSet {
        val javaConvention = project.convention.getPlugin(JavaPluginConvention::class.java)
        val main = javaConvention.sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)
        val test = javaConvention.sourceSets.getByName(SourceSet.TEST_SOURCE_SET_NAME)
        return javaConvention.sourceSets.create(KubeIntPlugin.ITEST_TASK_NAME) {
            it.compileClasspath += test.output + main.output + test.compileClasspath
            it.runtimeClasspath += test.output + main.output + test.runtimeClasspath

            // compileItestJava must depend on processITestResources
            project.tasks.getByName(it.compileJavaTaskName).dependsOn(project.tasks.getByName(it.processResourcesTaskName))
        }
    }

    /**
     * Configures the Itest settings
     */
    private fun setupConfiguration(project: Project) {
        val capitalizedName = KubeIntPlugin.ITEST_TASK_NAME.capitalize()
        project.configurations.getByName("${KubeIntPlugin.ITEST_TASK_NAME}Implementation") {
            it.extendsFrom(project.configurations.getByName("testImplementation"))
            it.isVisible = true
            it.isTransitive = true
            it.description = "$capitalizedName Implementation"
        }
        if (project.configurations.names.contains("testRuntimeOnly")) {
            // gradle 7
            project.configurations.getByName("${KubeIntPlugin.ITEST_TASK_NAME}RuntimeOnly") {
                it.extendsFrom(project.configurations.getByName("testRuntimeOnly"))
                it.isVisible = true
                it.isTransitive = true
                it.description = "$capitalizedName Runtime Only"
            }
        } else {
            // gradle < 7
            project.configurations.getByName("${KubeIntPlugin.ITEST_TASK_NAME}Runtime") {
                it.extendsFrom(project.configurations.getByName("testRuntime"))
                it.isVisible = true
                it.isTransitive = true
                it.description = "$capitalizedName Runtime"
            }
        }
    }

    /**
     * Skip unit tests if `skipTests` or `skipUnitTests` flags are present.
     */
    private fun testTaskConfiguration(project: Project) {
        val testTask = project.tasks.getByName(JavaPlugin.TEST_TASK_NAME)
        testTask.onlyIf { !skipTest() }
    }

    /**
     * Sets up configuration to run both unit/integration tests and `skipTests` property
     */
    private fun testAllTaskConfiguration(project: Project) {
        val testAllTask = project.tasks.create(KubeIntPlugin.TEST_ALL_TASK_NAME)
        testAllTask.description = "Runs all tests."
        testAllTask.group = LifecycleBasePlugin.VERIFICATION_GROUP
        testAllTask.onlyIf { !KubeIntPropertyHandler.skipTestAll() }
        project.tasks.withType(Test::class.java).forEach {
            testAllTask.dependsOn(it.name)
        }
    }
}
