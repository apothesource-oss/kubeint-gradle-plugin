package com.apothesource.fam.kubeint

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.internal.provider.DefaultProviderFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import java.net.InetAddress


/**
 * Gradle Plugin Extension for passing in configuration properties
 */
open class KubeIntExtension(project: Project) {
    private val namespace: Property<String>
    private val projectImage: Property<String>
    private val testFile: Property<String>
    private val deployTimeout: Property<Long>
    private val deleteTimeout: Property<Long>
    private val service: ServiceParameter

    fun service(action: Action<in ServiceParameter>) {
        action.execute(service)
    }

    fun getNamespace(): String {
        return namespace.get()
    }

    fun setNamespace(namespace: String?) {
        this.namespace.set(namespace)
    }

    fun getProjectImage(): String {
        return projectImage.get()
    }

    fun setProjectImage(projectImage: String?) {
        this.projectImage.set(projectImage)
    }

    fun getTestFile(): String {
        return testFile.get()
    }

    fun setTestFile(testFile: String?) {
        this.testFile.set(testFile)
    }

    fun getDeployTimeout(): Long {
        return deployTimeout.get()
    }

    fun setDeployTimeout(deployTimeout: Long?) {
        this.deployTimeout.set(deployTimeout)
    }

    fun getDeleteTimeout(): Long {
        return deleteTimeout.get()
    }

    fun setDeleteTimeout(deleteTimeout: Long?) {
        this.deleteTimeout.set(deleteTimeout)
    }

    @Nested
    fun getService(): ServiceParameter {
        return service
    }

    /**
     * Strips and returns {@code string} with non-alphanumeric (or hyphen) characters removed and any trailing characters over {@code length}.
     */
    fun strip(string: String, length: Int = 64): String {
        return string
            .replace("^[0-9]+", "")           // remove anything non-alphanumeric initial characters,
            .replace("[^a-zA-Z0-9-]".toRegex(), "")  // and any characters not alphanumeric or dash
            .take(length)
            .replace("-+$".toRegex(), "")            // remove any trailing dashes
    }

    init {
        DefaultProviderFactory()
        val objectFactory = project.objects
        val hostname = strip(InetAddress.getLocalHost().hostName, 56 - project.name.length)
        namespace = objectFactory.property(String::class.java).convention("${project.name}-test-${hostname}")
        projectImage = objectFactory.property(String::class.java)
        testFile = objectFactory.property(String::class.java).convention("kube.yaml")
        deployTimeout = objectFactory.property(Long::class.java).convention(120L)
        deleteTimeout = objectFactory.property(Long::class.java).convention(120L)
        service = objectFactory.newInstance(ServiceParameter::class.java)
    }
}
