package com.apothesource.fam.kubeint

import com.apothesource.fam.kubeint.KubeIntPropertyHandler.ServiceMode.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test


internal class KubeIntPropertyHandlerTest {

    @Test
    fun toServiceMode() {
        assertThat(KubeIntPropertyHandler.toServiceMode("forward")).isEqualTo(FORWARD)
        assertThat(KubeIntPropertyHandler.toServiceMode("FORWARD")).isEqualTo(FORWARD)

        assertThat(KubeIntPropertyHandler.toServiceMode("clusterIP")).isEqualTo(CLUSTERIP)
        assertThat(KubeIntPropertyHandler.toServiceMode("CLUSTERIP")).isEqualTo(CLUSTERIP)

        assertThat(KubeIntPropertyHandler.toServiceMode("something")).isEqualTo(NODEPORT)
        assertThat(KubeIntPropertyHandler.toServiceMode("")).isEqualTo(NODEPORT)
        assertThat(KubeIntPropertyHandler.toServiceMode(null)).isEqualTo(NODEPORT)
        assertThat(KubeIntPropertyHandler.toServiceMode("null")).isEqualTo(NODEPORT)
        assertThat(KubeIntPropertyHandler.toServiceMode("nodeport")).isEqualTo(NODEPORT)
    }
}