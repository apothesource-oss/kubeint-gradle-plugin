package com.apothesource.fam.kubeint

import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.util.function.Consumer

class KubeIntPluginTest {

    @Rule
    @JvmField  // necessary to workaround "@Rule ... must be public" error
    var testProjectRoot: TemporaryFolder = TemporaryFolder()

    @Test
    fun testProject() {
        val project = createProject("java", "com.apothesource.fam.kubeint.itest")
        val tasks = project.tasks
        val itestTask = tasks.getByPath(":itest")

        assertThat(itestTask).isNotNull
        assertThat(itestTask.taskDependencies.getDependencies(itestTask)).isNotEmpty

        val dependencies =
            itestTask.taskDependencies.getDependencies(itestTask).stream().map { it.name }
        assertThat(dependencies).contains(KubeIntPlugin.DEPLOY_SERVICES)
    }

    private fun createProject(vararg plugins: String): Project {
        val project = ProjectBuilder.builder().withProjectDir(testProjectRoot.root).withName("root").build()
        listOf(*plugins).forEach(Consumer { pluginId: String ->
            project.pluginManager.apply(pluginId)
        })
        return project
    }
}