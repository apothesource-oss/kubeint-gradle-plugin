package com.apothesource.fam.kubeint

import org.assertj.core.api.Assertions.assertThat
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Test

internal class KubeIntExtensionTest {

    private var extension: KubeIntExtension? = null

    @Before
    fun setUp() {
        val fakeProject = ProjectBuilder.builder().build()
        extension = fakeProject.extensions
            .create(KubeIntPlugin.EXTENSION_NAME, KubeIntExtension::class.java, fakeProject)
    }

    @Test
    fun testService() {
        assertThat(extension?.getService()).isNotNull

        extension?.service { service ->
            service.setName("my-service-v1")
            service.setLocalPort(123)
            service.setRemotePort(456)
        }

        assertThat(extension?.getService()).isNotNull
        assertThat(extension?.getService()?.getName()).isNotNull
        assertThat(extension?.getService()?.getRemotePort()).isNotNull
        assertThat(extension?.getService()?.getLocalPort()).isNotNull
    }

    @Test
    fun testStrip() {
        assertThat(extension?.strip("host")).isEqualTo("host")
        assertThat(extension?.strip("host-the-most")).isEqualTo("host-the-most")
        assertThat(extension?.strip("abc-", 100)).isEqualTo("abc")
        assertThat(extension?.strip("host.local")).isEqualTo("hostlocal")
        assertThat(extension?.strip("10.10.10.10")).isEqualTo("10101010")
        assertThat(extension?.strip("abcdefghijklmnopqrstuvwxyz", 5)).isEqualTo("abcde")
        assertThat(extension?.strip("abcdefghijklmnopqrstuvwxyz", 100)).isEqualTo("abcdefghijklmnopqrstuvwxyz")
    }

}