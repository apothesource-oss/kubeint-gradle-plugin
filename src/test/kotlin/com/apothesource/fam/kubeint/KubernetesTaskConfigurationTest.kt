package com.apothesource.fam.kubeint

import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import org.assertj.core.api.Assertions.assertThat

internal class KubernetesTaskConfigurationTest {

    @Test
    fun testRegisterDeleteNamespace() {
        // Create a test project and apply the plugin
        val project = ProjectBuilder.builder().build()
        project.plugins.apply("com.apothesource.fam.kubeint.itest")

        val task = project.tasks.findByName("deleteNamespace")

        assertThat(task).isNotNull;
        assertThat(task?.enabled).isTrue
    }

}