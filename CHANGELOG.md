# 0.5.0
* Fix for trailing dash in namespace.
* Add `http://` to clusterip resolved addresses.
* Fix ClusterIP port.
* Fix namespace deployment to wait for pods to be ready.
* Add logging.
* Add delay for ClusterIP services

# 0.4.0
* Change `forward` to `service`.
* Add `KubeIntServiceValues`.

# 0.3.2
* Restrict namespace name length.

# 0.3.1
* Remove elvis.

# 0.3.0
* Remove illegal characters from generated namespace name.

# 0.2.1
* Refactoring for tests.

# 0.2.0
* Add the client hostname to the k8s namespace id.

# 0.1.0
* Add port-forwarding as a replacement for `nodePort` services.
* Tune status backoffs to speed up task completion.

# 0.0.7
* Add `saveTestNamespace` condition to `deleteNamespace`'s `onlyIf` clause.

# 0.0.6
* Skip `deleteNamepace` finalizer if `deployServices` didn't do work.
* Add `onlyIf` conditions to tasks.

# 0.0.5
* Change `gitlab-docker-config` secret to `docker-config`
* Remove `skipJib` property
* Remove task dependency on jib
* Create group for tasks.
* Add finalizer for itest to delete the namespace.

# 0.0.4
* Use dependency graph to determine if `jibDockerBuild` should be used
* Remove `useJibDockerBuild` property
    
# 0.0.3
* Add ability to skip jib completely
* Add ability to switch between `jib` and `jibDockerBuild`
* Add ability to skip deploying the services

# 0.0.2
* Update how tasks get registered to avoid jib being registered when not needed
* Update deleteNamespace task to avoid running when itests are skipped

# 0.0.1
* Initial Commit