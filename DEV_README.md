# KubeInt Gradle Plugin Local Development Instructions

## Building the plugin
In order to test the plugin locally you will need to run a `./gradlew clean check publishToMavenLocal`.
This will build the plugin and then publish to the local `.m2` repo on your machine

## Testing
In order to test the plugin, you will need to use the legacy buildscript in order to pull the latest version from
your local `.m2` repo. You will need to setup your test project with the following:

```groovy
buildscript {
    repositories {
        mavenLocal() // plugin published to maven local
    }
    dependencies {
        classpath 'com.apothesource.fam:kubeint-gradle-plugin:<version>' 
    }
}
 plugins {
     ...
 }

apply plugin: 'com.apothesource.fam.kubeint.itest'
```
Note: Be sure to remove or comment out the existing plugin definition in the `plugins` section of your build script

Then the config from the README can be applied
